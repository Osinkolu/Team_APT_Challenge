#Check Time
import time
start_time = time.time()

#Create basline for Providers
dictionary = {"MTN": ["0703","0706","0803","0806","0810","0813","0814","0816","0903","0906","0913","0916","0702","0704"],
"Airtel" :["0701","0708","0802","0808","0812","0901","0902","0904","0907","0912"],
"Globacom" :["0705","0805","0807","0811","0815","0905","0915"],
"9Mobile" :["0809","0817","0818","0909","0908"],
"MTEL" : ["0804"]}


# function to check phone number
def get_key(val):
    for key, value in dictionary.items():
         if val in value:
             return key

#Function to give report
def report_providers(filename):
    file = open(filename, "r")
    rows = (row.strip().split() for row in file)
    cut_four = [i[0][:4] for i in list(rows)]
    providers = [get_key(i) for i in cut_four]

    print("The Number of MTN phone numbers are", providers.count("MTN"))
    print("The Number of Airtel phone numbers are",providers.count("Airtel"))
    print("The Number of Globacom phone numbers are",providers.count("Globacom"))
    print("The Number of 9Mobile phone numbers are",providers.count("9Mobile"))
    print("The Number of MTEL phone numbers are",providers.count("MTEL"),"\n")
    print("The time taken to run is %s seconds " % (time.time() - start_time))

#pass the filename to get report.
report_providers("PhoneNumbers.txt")